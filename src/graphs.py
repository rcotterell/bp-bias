import seaborn as sb
from matplotlib import pyplot as plt
import pandas as pd

# Spanish
fem_bias_es = [3.793189207712809, 1.235958456993103, 1.6366414626439412]
masc_bias_es = [3.497094470101434, 1.402841039606043, -0.8660545477996001]
grammar_es = [2.9171551295689175, 1.491630320383315, 2.9814630714622705]

# French
fem_bias_fr = [4.025381965637207, 4.829296112060547, 7.101934661865235]
masc_bias_fr = [4.059191452213591, 4.578427700908637, -0.13692039653567448]
grammar_fr = [1.3973983672296577, 1.1844796097796897, 1.3286718492922576]

# Hebrew
fem_bias_he = [2.650020122528076, 2.6431661519137295, 2.3794485438953745]
masc_bias_he = [6.268900410603669, 7.065820257542497, 3.780371075969631]
grammar_he = [1.1424208643535774, 1.1045052967965603, 0.7845901815841595]

cat = ["Original", "Swap", "De-biased", "Original", "Swap", "De-biased", "Original", "Swap", "De-biased"]

lang = ["French", "French", "French", "Hebrew", "Hebrew", "Hebrew", "Spanish", "Spanish", "Spanish"]

grammar = grammar_fr + grammar_he + grammar_es
fem_bias = fem_bias_fr + fem_bias_he + fem_bias_es
masc_bias = masc_bias_fr + masc_bias_he + masc_bias_es


data = pd.DataFrame(data={"Language": lang, "Grammar": grammar, "Source": cat, "Masc-bias": masc_bias, "Fem-bias": fem_bias})
# data2 = pd.DataFrame(data=d2)

# tips = sb.load_dataset("tips")
# print(tips)

sb.set(style="whitegrid")

color_pal = sb.color_palette()
colors = [color_pal[1], color_pal[4], color_pal[2]]
# color_pal = sb.color_palette(colors)

sb.set(font='serif', font_scale=0.8)

_, ax = plt.subplots(figsize=[3.1, 3.5])
ax = sb.barplot(ax=ax, x='Language', y='Grammar', hue='Source', data=data, palette=colors)
plt.xlabel("")
plt.ylabel("Average grammaticality")
plt.title("Grammaticality of Multi-Lingual Corpora")
plt.tight_layout()
plt.savefig("grammar.pdf")

_, ax = plt.subplots(figsize=[3.1, 3.5])
ax = sb.barplot(ax=ax, x='Language', y='Masc-bias', hue='Source', data=data, palette=colors)
plt.xlabel("")
plt.ylabel("Average masculine bias")
plt.title("Masculine bias of Multi-Lingual Corpora")
plt.tight_layout()
plt.savefig("masc_bias.pdf")

_, ax = plt.subplots(figsize=[3.1, 3.5])
ax = sb.barplot(ax=ax, x='Language', y='Fem-bias', hue='Source', data=data, palette=colors)
plt.xlabel("")
plt.ylabel("Average feminine bias")
plt.title("Feminine bias of Multi-Lingual Corpora")
plt.tight_layout()
plt.savefig("fem_bias.pdf")