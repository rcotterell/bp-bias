echo "Getting French reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_French/fr-ud-train.conllu --out_file reinflection/french/french-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_French/fr-ud-dev.conllu --out_file reinflection/french/french-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_French/fr-ud-test.conllu --out_file reinflection/french/french-reinflection-test
echo "Getting Italian reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Italian/it-ud-train.conllu --out_file reinflection/italian/italian-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Italian/it-ud-dev.conllu --out_file reinflection/italian/italian-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Italian/it-ud-test.conllu --out_file reinflection/italian/italian-reinflection-test
echo "Getting Portuguese reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Portuguese/pt-ud-train.conllu --out_file reinflection/portuguese/portuguese-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Portuguese/pt-ud-dev.conllu --out_file reinflection/portuguese/portuguese-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Portuguese/pt-ud-test.conllu --out_file reinflection/portuguese/portuguese-reinflection-test
echo "Getting German reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_German/de-ud-train.conllu --out_file reinflection/german/german-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_German/de-ud-dev.conllu --out_file reinflection/german/german-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_German/de-ud-test.conllu --out_file reinflection/german/german-reinflection-test
echo "Getting Greek reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Greek/el-ud-train.conllu --out_file reinflection/greek/greek-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Greek/el-ud-dev.conllu --out_file reinflection/greek/greek-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Greek/el-ud-test.conllu --out_file reinflection/greek/greek-reinflection-test
echo "Getting Arabic reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Arabic/ar-ud-train.conllu --out_file reinflection/arabic/arabic-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Arabic/ar-ud-dev.conllu --out_file reinflection/arabic/arabic-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Arabic/ar-ud-test.conllu --out_file reinflection/arabic/arabic-reinflection-test
echo "Getting Polish reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Polish/pl-ud-train.conllu --out_file reinflection/polish/polish-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Polish/pl-ud-dev.conllu --out_file reinflection/polish/polish-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Polish/pl-ud-test.conllu --out_file reinflection/polish/polish-reinflection-test
echo "Getting Russian reinflection documents"
python src/get_reinflection_files.py --in_file ud/UD_Russian/ru-ud-train.conllu --out_file reinflection/russian/russian-reinflection-train
python src/get_reinflection_files.py --in_file ud/UD_Russian/ru-ud-dev.conllu --out_file reinflection/russian/russian-reinflection-dev
python src/get_reinflection_files.py --in_file ud/UD_Russian/ru-ud-test.conllu --out_file reinflection/russian/russian-reinflection-test

