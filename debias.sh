#echo "greek"
#python src/main.py --in_files greek.conllu --psi models/psi_grk/psi_1.180883_1.287951_epoch45.pt --reinflect models/greek.nll_0.0302.acc_93.1246.dist_0.7557.epoch_19 --animate_list animacy/greek_animacy --inc_input --get_ids --use_v1 --out_file greek_debias.conllu --part 100
#echo "german"
#python src/main.py --in_files german.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_ger/psi_0.308049_0.241209_epoch6.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/german.nll_0.0279.acc_95.585.dist_0.0662.epoch_14 --animate_list animacy/german_animacy --inc_input --get_ids --use_v1 --out_file german_debias.conllu --part 100
echo "french"
python src/main.py --in_files french.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_fr/psi_1.163777_1.207678_epoch11.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/french.nll_0.0248.acc_96.7339.dist_0.0734.epoch_15 --animate_list animacy/french_animacy --inc_input --get_ids --use_v1 --out_file french_debias.conllu --part 100
echo "italian"
python src/main.py --in_files italian.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_it/psi_1.496631_1.634817_epoch12.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/italian.nll_0.0088.acc_98.1925.dist_0.0347.epoch_19 --animate_list animacy/italian_animacy --inc_input --get_ids --use_v1 --out_file italian_debias.conllu --part 100
#echo "portuguese"
#python src/main.py --in_files portuguese.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_por/psi_1.543623_1.265956_epoch15.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/portuguese.nll_0.0214.acc_97.3022.dist_0.1601.epoch_19 --animate_list animacy/portuguese_animacy --inc_input --get_ids --use_v1 --out_file portuguese_debias.conllu --part 100
echo "polish"
python src/main.py --in_files polish.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_pol/psi_0.852242_0.793031_epoch12.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/polish.nll_0.0289.acc_96.3694.dist_0.0718.epoch_19 --animate_list animacy/polish_animacy --inc_input --get_ids --use_v1 --out_file polish_debias.conllu --part 100
echo "russian"
python src/main.py --in_files russian.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_rus/psi_2.146904_2.165452_epoch39.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/russian.nll_0.0354.acc_95.8525.dist_0.0553.epoch_17 --animate_list animacy/russian_animacy --inc_input --get_ids --use_v1 --out_file russian_debias.conllu --part 100
echo "arabic"
python src/main.py --in_files arabic.conllu --psi /home/ran/Documents/year4/project/bp-bias/models/psi_arb/psi_2.440265_2.408666_epoch28.pt --reinflect /home/ran/Documents/year4/project/bp-bias/models/arabic.nll_0.0566.acc_86.1423.dist_0.1873.epoch_19 --animate_list animacy/arabic_animacy --inc_input --get_ids --use_v1 --out_file arabic_debias.conllu --part 100
