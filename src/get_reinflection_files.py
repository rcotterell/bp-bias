if __name__ == "__main__":
    from argparse import ArgumentParser
    from utils.reinflection import create_reinflection_file

    p = ArgumentParser()
    p.add_argument('--in_file', required=True, type=str)
    p.add_argument('--out_file', required=True, type=str)

    args = p.parse_args()

    create_reinflection_file(args.in_file, args.out_file)