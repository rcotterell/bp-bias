#!/bin/bash
arch=$1
train=$2
dev=$3
model=$4
python src/sigmorphon_reinflection/train.py \
    --dataset sigmorphon19task1 \
    --train $train  \
    --dev $dev \
    --model $model --seed 0 \
    --embed_dim 200 --src_hs 400 --trg_hs 400 --dropout 0.4 \
    --src_layer 2 --trg_layer 1 --max_norm 5 \
    --arch $arch --estop 1e-8 --epochs 50 --bs 20 --mono