#echo "Training French reinflector"
#python src/reinflection_train.py --arch hmmfull --train reinflection/french/french-reinflection-train --dev reinflection/french/french-reinflection-dev --model models/french --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-8 --bs 20 --dataset sigmorphon19task1 --mono
echo "Training Italian reinflector"
python src/reinflection_train.py --arch hmmfull --train reinflection/italian/italian-reinflection-train --dev reinflection/italian/italian-reinflection-dev --model models/italian --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
echo "Training Portuguese reinflector"
python src/reinflection_train.py --arch hmmfull --train reinflection/portuguese/portuguese-reinflection-train --dev reinflection/portuguese/portuguese-reinflection-dev --model models/portuguese --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
echo "Training German reinflector"
python src/reinflection_train.py --arch hmmfull --train reinflection/german/german-reinflection-train --dev reinflection/german/german-reinflection-dev --model models/german --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
#echo "Training Greek reinflector"
#python src/reinflection_train.py --arch hmmfull --train reinflection/greek/greek-reinflection-train --dev reinflection/greek/greek-reinflection-dev --model models/greek --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
#echo "Training Arabic reinflector"
#python src/reinflection_train.py --arch hmmfull --train reinflection/arabic/arabic-reinflection-train --dev reinflection/arabic/arabic-reinflection-dev --model models/arabic --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
#echo "Training Polish reinflector"
#python src/reinflection_train.py --arch hmmfull --train reinflection/polish/polish-reinflection-train --dev reinflection/polish/polish-reinflection-dev --model models/polish --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono
#echo "Training Russian reinflector"
#python src/reinflection_train.py --arch hmmfull --train reinflection/russian/russian-reinflection-train --dev reinflection/russian/russian-reinflection-dev --model models/russian --seed 0 --src_hs 400 --trg_hs 400 --dropout 0.4 --src_layer 2 --trg_layer 1 --max_norm 5 --estop 1e-6 --bs 20 --dataset sigmorphon19task1 --mono

