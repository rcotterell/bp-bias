import argparse
from nltk.corpus import wordnet as wn
from utils.conll import load_sentences

parser = argparse.ArgumentParser()
parser.add_argument('--tab')
parser.add_argument('--conll')
parser.add_argument('--output')
parser.add_argument('--part', default=100, type=int)
args = parser.parse_args()

person = wn.synsets('person')[0]


def get_hypernyms(synset):
    hypernyms = set()
    for hypernym in synset.hypernyms():
        hypernyms |= set(get_hypernyms(hypernym))
    return hypernyms | set(synset.hypernyms())


offset_dict = dict()
with open(args.tab, 'r') as f:
    lines = f.readlines()[1:]
for line in lines:
    line = line.strip()
    split = line.split("\t")
    if len(split) != 3:
        continue
    offset, _, word = line.split("\t")
    word = word[0].lower() + word[1:]
    offset, pos = offset.split("-")
    if pos != "n":
        continue
    if offset not in offset_dict:
        offset_dict[offset] = set()
    offset_dict[offset].add(word)
unordered_pairs = []
for offset_id in offset_dict:
    offset = int(offset_id)
    synset = wn.synset_from_pos_and_offset(wn.NOUN, offset)
    if person in get_hypernyms(synset):
        english = synset.lemmas()[0].name()
        if len(offset_dict[offset_id]) < 2:
            continue
        prefixes = []
        for word in offset_dict[offset_id]:
            if "-" in word or " " in word or "־" in word:
                continue
            prefix_len = len(word) // 2 + (len(word) % 2)
            prefix = word[:prefix_len]
            prefix_exists = False
            for pre in prefixes:
                if prefix.startswith(pre) or pre.startswith(prefix):
                    prefix_exists = True
                    break
            if prefix_exists:
                continue
            prefixes.append(prefix)
            found = False
            for word2 in offset_dict[offset_id]:
                if word == word2 or "-" in word2 or " " in word2 or "־" in word2:
                    continue
                elif word2.startswith(prefix):
                    if found:
                        print(word, word2, unordered_pairs[-1], "BADD")
                        continue
                    unordered_pairs.append((english, word, word2))
                    found = True
f = open(args.conll, "r")
pairs = []
part = 0
while unordered_pairs and part < args.part:
    part += 1
    print("Partition", part, ":", len(pairs), "pairs")
    conll, not_empty = load_sentences(10000, f)
    for sent in conll:
        for tok in sent:
            if tok.is_multiword() or tok.upos != "NOUN" or "Gender" not in tok.feats or len(tok.feats["Gender"]) != 1:
                continue
            lemma = tok.lemma
            idx = -1
            for i in range(len(unordered_pairs)):
                english, word1, word2 = unordered_pairs[i]
                if lemma == word1 or lemma == word2:
                    gender = next(iter(tok.feats['Gender']))
                    if gender != "Fem" and gender != "Masc":
                        continue
                    if (gender == "Fem" and lemma == word1) or (gender == "Masc" and lemma == word2):
                        fem, masc = word1, word2
                    else:
                        fem, masc = word2, word1
                    pairs.append((english, fem, masc))
                    idx = i
                    break
            if idx >= 0:
                unordered_pairs.pop(idx)
    if not not_empty:
        raise ValueError("File out of sentences!")

with open(args.output, 'w') as f:
    f.write("\n".join(["\t".join(line) for line in pairs]))
