echo "Greek"
python src/wordnet.py --tab data/wn-wikt-ell.tab --output animacy/greek_animacy --conll greek.conllu 
echo "arabic"
python src/wordnet.py --tab data/wn-wikt-arb.tab --output animacy/arabic_animacy --conll arabic.conllu 
echo "polish"
python src/wordnet.py --tab data/wn-wikt-pol.tab --output animacy/polish_animacy --conll polish.conllu 
echo "portuguese"
python src/wordnet.py --tab data/wn-wikt-por.tab --output animacy/portuguese_animacy --conll portuguese.conllu
echo "russian"
python src/wordnet.py --tab data/wn-wikt-rus.tab --output animacy/russian_animacy --conll russian.conllu
echo "german"
python src/wordnet.py --tab data/wn-wikt-deu.tab --output animacy/german_animacy --conll german.conllu 
