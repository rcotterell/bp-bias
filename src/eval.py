import numpy as np


def eval_sentence(original, truth, predicted):
    """
    Evaluate predicted sentence against gold sentence on only those words which could get changed (have gender)
    :param original: original sentence
    :param truth: gold sentence
    :param predicted: predicted sentence
    """
    tp_tag = fp_tag = fn_tag = 0
    form_acc = form_tot = 0
    for i in range(len(original)):
        tok_o = original[i]
        if tok_o.is_multiword() or 'Gender' not in tok_o.feats or len(tok_o.feats['Gender']) != 1:
            continue
        tok_t = truth[i]
        tok_p = predicted[i]
        assert 'Gender' in tok_t.feats and 'Gender' in tok_p.feats, original.id + " " + truth.id + " " + predicted.id + "\n" +\
                                                                    tok_o.conll() + "\n" + tok_t.conll() + "\n" + tok_p.conll() + "\n"
        # Gender tag check
        gender_o = next(iter(tok_o.feats['Gender']))
        gender_t = next(iter(tok_t.feats['Gender']))
        gender_p = next(iter(tok_p.feats['Gender']))
        if gender_o == gender_t:
            if gender_t != gender_p:
                fp_tag += 1
        else:
            if gender_t == gender_p:
                tp_tag += 1
            else:
                fn_tag += 1
        # Form check
        form_t = tok_t.form.strip()
        form_p = tok_p.form.strip()
        form_tot += 1
        form_acc += form_t == form_p
    return tp_tag, fp_tag, fn_tag, form_acc, form_tot


def evaluate(conll_original, conll_truth, conll_pred, heb):
    triples = dict()
    for sent in conll_truth:
        triples[sent.id] = {'truth': sent}
    for sent in conll_pred:
        if sent.id in triples:
            triples[sent.id]['pred'] = sent
    for sent in conll_original:
        for sent_id in triples:
            x = sent_id.split("-")
            if (heb and x[0] == sent.id) or (not heb and "-".join(x[:4]) == sent.id):
                triples[sent_id]['original'] = sent
    remove = []
    for key in triples:
        if len(triples[key]) != 3:
            remove.append(key)
    for key in remove:
        del triples[key]
    tp_tag = fp_tag = fn_tag = acc_form = tot_form = 0
    for sent_id in triples:
        sample = triples[sent_id]
        tp_t, fp_t, fn_t, acc_f, tot_f = eval_sentence(sample['original'], sample['truth'], sample['pred'])
        tp_tag += tp_t
        fp_tag += fp_t
        fn_tag += fn_t
        acc_form += acc_f
        tot_form += tot_f
    prec_tag, recall_tag = tp_tag / (tp_tag + fp_tag), tp_tag / (tp_tag + fn_tag)
    f1_tag = 2 * prec_tag * recall_tag / (prec_tag + recall_tag)
    return prec_tag, recall_tag, f1_tag, acc_form / tot_form


